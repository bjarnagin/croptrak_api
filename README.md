# ~ CropTrak API ~

### What is it?
This library facilitates interaction with the CropTrak REST API

### Is it ready?
Nope.

### What do I need?
Use a virtualenv. Package will be uploaded to PyPI once it's building.

### Where are the docs?
TBD: Gitlab Pages

### Where can I get help?
Email Billy Jarnagin at billy@cogent3d.com

### Examples
```python
credentials = dict(
    username="your_username",
    password="your_password"
)

import croptrak_api as api

# Create an API Manager instance. This will log in to the REST API and acquire a list
# of valid targets to execute API queries against.
m = api.Manager(**credentials)

# Display a list of valid API targets
m.targets
# (<Target name=BrandonCities2_iCropTrak>, <Target name=LaureBecks_iCropTrak>)


# Query an API target for top-level assets
api.Query(m.targets[0]).get('Assets', params={'parent_guid': -100})
# [
#    {
#         'asset_type': 'City', 
#         'guid': '16fc0a7d-35a1-4b21-870b-d34f2fffe9cf', 
#         'name': 'Phoenix', 
#         'user_tag': 'tag', 
#         'geometry': None, 
#         'parent_guid': '-100'
#     }, 
#     {
#         'asset_type': 'City', 
#         'guid': '83f1ea3a-9872-4e52-ad66-31690aa080f7', 
#         'name': 'Tucson', 
#         'user_tag': 'Tucson', 
#         'geometry': None, 
#         'parent_guid': '-100'
#     }, ...
# ]

# There is also a helper method for the same command:
api.Query(m.targets[0]).assets(params={'parent_guid': -100})
```