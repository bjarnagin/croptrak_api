from setuptools import setup, find_packages

setup(
    name = "CropTrakAPI",
    version = "0.1",
    packages = find_packages()

    author = "Billy Jarnagin",
    author_email = "billy@cogent3d.com",
    description = "This package facilitates interaction with the CropTrak REST API",
    license = "MIT",
    keywords = "CropTrak Cogent3D",
    url = 'https://gitlab.com/bjarnagin/croptrak_api'
)