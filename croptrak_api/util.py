import re

def camel_to_snake(s):
    a = re.compile('((?<=[a-z0-9])[A-Z]|(?!^)[A-Z](?=[a-z]))')
    return a.sub(r'_\1', s).lower()

def snake_to_camel(name, first_cap=False):
    components = name.split('_')
    if first_cap == True:
        str = "".join(x.title() for x in components)
    else:
        str = components[0] + "".join(x.title() for x in components[1:])
    return str

# Adapted from StackOverflow question:
# http://stackoverflow.com/a/21742678
def convert_json(f, j):
    result = {}
    for k in j:
        new_k = f(k)
        if isinstance(j[k], dict):
            result[new_k] = convert_json(f, j[k])
        elif isinstance(j[k],list):
            result[new_k] = convert_array(f, j[k])
        else:
            result[new_k] = j[k]
    return result

def convert_array(f, a):
    result = []
    for i in a:
        if isinstance(i, list):
            result.append(convert_array(f, i))
        elif isinstance(i, dict):
            result.append(convert_json(f, i))
        else:
            result.append(i)
    return result