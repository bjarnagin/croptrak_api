from collections import namedtuple

class Asset(namedtuple('Asset', [
    'guid', 
    'name', 
    'asset_type', 
    'user_tag', 
    'parent_guid', 
    'geometry'])):

    # Setting __slots__ to an empty tuple ensures class instances only take
    # as much memory as a regular tuple. Omitting this instantiates a class
    # dict, which is much larger.
    __slots__ = ()

    def geojson(self, properties=True, as_feature=True, bbox=None):
        """Creates a GeoJson object from asset"""

        if self.geometry is None or self.geometry == "":
            return None

        if as_feature == False:
            return self.geometry

        feature = {
            'type': 'Feature',
            'geometry': self.geometry,
            'properties': {
                p: getattr(self, p) for p in self._fields if p != 'geometry'
            }
        }

        if bbox is not None:
            feature['bbox'] = bbox

        return feature

    def __geo_interface__(self):
        return self.geojson()

    def __repr__(self):
        return 'Asset(type={asset_type} name={name})'.format(
            asset_type=self.asset_type, name=self.name)
