import requests
from datetime import datetime, timedelta

from .routes import Routes
from .target import Target

class Manager:
    def __init__(self, username, password):
        self._username = username
        self._password = password
        self._targets = self._get_targets()
        self._initialized_at = datetime.utcnow()
        self._expiration = self._initialized_at + timedelta(hours=12)

    @property
    def username(self):
        return self._username

    @property
    def targets(self):
        return self._targets

    @property
    def initialized_at(self):
        return self._initialized_at

    @property
    def expiration(self):
        return self._expiration

    @property
    def status(self):
        return self._get_status()

    def _get_status(self):
        elapsed = (datetime.utcnow() - self._initialized_at).total_seconds()
        if (elapsed / 60 / 60) <= 12:
            return 'Active'
        else:
            return 'Expired'

    def _get_targets(self):
        api_credentials = dict(username=self._username, password=self._password)
        r = requests.post(Routes.get('Login'), data=api_credentials)

        # Raise an exception if r.status_code != 200
        r.raise_for_status()

        return tuple(
            Target(tt['TargetName'], tt['Region'], tt['TargetToken']) 
            for tt in r.json()
        )
            

    def refresh_targets(self):
        self._targets = self._get_targets()

    def __repr__(self):
        return 'Manager(user={user} status={status})'.format(
            user=self._username, 
            status=self._get_status()
        )