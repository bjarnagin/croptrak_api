import requests
from functools import partialmethod

from .routes import Routes
from .target import Target
from .util import convert_array, camel_to_snake

class Query:
    def __init__(self, target):
        if not isinstance(target, Target):
            raise TypeError('Query target must be of class Target')

        self._target = target

    @property
    def target(self):
        return self._target

    def get(self, route, **params):
        url = Routes.get(route)

        r = requests.get(url, headers=self.target.auth_payload(), params=params)

        # Raise an exception if r.status_code != 200
        r.raise_for_status()

        return convert_array(camel_to_snake, r.json())

    assets = partialmethod(get, 'Assets')

    def __repr__(self):
        return 'Query(target={target})'.format(target=self.target.name)