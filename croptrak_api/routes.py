class Routes:
    __slots__ = ()
    
    URL = 'https://api.croptrak.com'
    ROUTES = ('Login','Assets')

    @staticmethod
    def get(route):
        if route not in Routes.ROUTES:
            raise KeyError('Invalid route: %s' % route)
        return '{url}/{route}'.format(url=Routes.URL, route=route)