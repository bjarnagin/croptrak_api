from collections import namedtuple

class Target(namedtuple('Target', 'name region token')):
    __slots__ = ()

    def auth_payload(self):
        "Formats the session token for use in requests"
        return {'Authorization': 'TargetToken {token}'.format(token=self.token)}

    def __str__(self):
        return self.token

    def __repr__(self):
        return 'Target(name={name}, region={region})'.format(
            name=self.name, 
            region=self.region)